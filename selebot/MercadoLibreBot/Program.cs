﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MercadoLibre;
using MagicSQL;

namespace MercadoLibreBot
{
    class Program
    {
        static void Main(string[] args)
        {
            //Inicializa todas las variables hasta que te dejen ir
            DataFirst.Bot _ProduccionBot = new DataFirst.Bot();
            _ProduccionBot.perfilNavegador = "default";
            _ProduccionBot.InicializarWebdriver();

            Thread.Sleep(5000);
            Console.WriteLine("Iniciando mensajero de MercadoLibre");
            while (true)
            {
                try
                {
                    Origen origen = new Origen().Select().OrderBy(x => x.FHUltimoUso).FirstOrDefault();
                    _ProduccionBot.IrA("https://listado.mercadolibre.com.ar/" + origen.Nombre.Replace(" ", "-"));
                    Console.WriteLine("Origen obtenido: "+ _ProduccionBot.WebDriver.Url);
                    int pagina = 1;
                    while (true)
                    {
                        try
                        {
                            string xpath = "/html/body/main/div[1]/div/section/div[2]/ul/li[11]/a/span[1]";
                            IWebElement btnSiguiente =
                            _ProduccionBot.
                                WebDriver.
                                    FindElement
                                        (
                                    By.XPath(xpath
                                        )
                                    );
                            btnSiguiente.Click();
                            pagina++;
                            Console.WriteLine("Visitando página " + pagina.ToString());
                            Console.WriteLine("Página obtenida: " + _ProduccionBot.WebDriver.Url);
                            Thread.Sleep(5000);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Maximo de páginas alcanzadas");
                            break;
                        }
                    }
                    Thread.Sleep(60000);
                }
                catch(Exception ex)
                {
                    break;
                }
            }
        }
    }
}
