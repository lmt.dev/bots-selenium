﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MagicSQL;
using Bet365;

namespace Bet365App
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Credencial credenciales =
                new Credencial().
                    Select().
                        Where
                            (it =>
                                it.Usuario == txtUsuario.Text
                                &&
                                it.Password == txtPassword.Text
                            ).FirstOrDefault();
            if (credenciales != null)
            {
                formCuentasExistentes formCuentas = new formCuentasExistentes();
                formCuentas.credencialesId = credenciales.IdCredencial;
                formCuentas.ShowDialog();
            }
            else
            {
                MessageBox.Show("Credenciales incorrectas, reintente nuevamente", "Error");
            }            
        }
    }
}
