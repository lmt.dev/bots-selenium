﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MagicSQL;
using Bet365;
using System.Diagnostics;

using OpenQA.Selenium;
using Modelo.NATS;
using NATS.Client;
using Newtonsoft.Json;
using System.Threading;
using Bet365Bot;

namespace Bet365App
{
    public partial class formCuentasExistentes : Form
    {

        //INICIO OBJETOS QUE FORMABAN PARTE DEL BOT DESACOPLADO

        //FIN OBJETOS QUE FORMABAN PARTE DEL BOT DESACOPLADO

        public formCuentasExistentes()
        {
            InitializeComponent();
        }

        public int credencialesId { get; set; }
        public static decimal CuotaMinima { get; set; }

        private void formCuentasExistentes_Load(object sender, EventArgs e)
        {
            List<Cuenta> listaCuentas =
                new Cuenta().
                    Select().
                        Where
                        (it =>
                            it.IdCredencial == credencialesId
                        ).ToList();
            foreach (Cuenta cuentaRecorrer in listaCuentas)
            {
                dgvCuentas.Rows.Add(cuentaRecorrer.Usuario, "Iniciar");
                Application.DoEvents();
            }

            Credencial credenciales = new Credencial().
                Select().
                    Where(
                        it=>
                            it.IdCredencial == credencialesId).FirstOrDefault();

            txtApuestaMinima.Text = credenciales.ApuestaMinima.ToString();
            Application.DoEvents();
        }

        static async void iniciarNavegacionApuestaAsync(string vUsuario, string vPassword, Bot b)
        {
            CuotaMinima = 1.7M;
            Bet365.Proxy bProx = new Bet365.Proxy();
            b.perfilNavegador = "bet365";
            b.InicializarWebdriver();
            b.CuotaMinima = CuotaMinima;
            Thread.Sleep(1000);
            b.IrA("https://www.bet365.com/#/IP/");
            Thread.Sleep(1000);
            b.IrA("https://www.bet365.com/#/IP/");
            Thread.Sleep(3000);

            if (vUsuario != null && vUsuario != string.Empty)
            {
                /*OJO: Para pruebas no reemplazar parametría, solo comentar la línea de abajo*/
                b.Main(vUsuario, vPassword);
                Application.DoEvents();
            }
            else
            {
                b.logBet("No se pudieron obtener las credenciales del usuario para iniciar sesión");
            }
        }

        private async void dgvCuentas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                MessageBox.Show("Iniciando apostador automático, aguarde unos segundos", "Atención");
                string vUsuario = dgvCuentas.Rows[e.RowIndex].Cells[0].Value.ToString();
                Cuenta cuentaIniciar = new Cuenta().
                    Select().
                        Where(it =>
                            it.IdCredencial == credencialesId
                            &&
                            it.Usuario == vUsuario
                        ).FirstOrDefault();

                if (cuentaIniciar != null)
                {
                    Bot bot = new Bot();
                    string paisConexionOrigen = bot.InfoIp();
                    bot.logBet("Pais de origen registrado: "+paisConexionOrigen);
                    Pais pais = new Pais().Select().Where(it => it.IdPais == cuentaIniciar.IdPais).FirstOrDefault();

                    //Lanzar el bot
                    iniciarNavegacionApuestaAsync(
                            cuentaIniciar.Usuario, cuentaIniciar.Password, bot
                        );

                    Application.DoEvents();
                }
                else
                {
                    MessageBox.Show("Se ha producido un error al obtener la cuenta de bet365","Error");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha fallado el inicio del apostador", "Atención");
            }  
        }

        private void txtApuestaMinima_KeyPress(object sender, KeyPressEventArgs e)
        {
            const int BACKSPACE = 8;
            const int DECIMAL_POINT = 46;
            const int ZERO = 48;
            const int NINE = 57;
            const int NOT_FOUND = -1;

            int keyvalue = (int)e.KeyChar; // not really necessary to cast to int

            if ((keyvalue == BACKSPACE) ||
            ((keyvalue >= ZERO) && (keyvalue <= NINE))) return;
            // Allow the first (but only the first) decimal point
            if ((keyvalue == DECIMAL_POINT) &&
            (txtApuestaMinima.Text.IndexOf(".") == NOT_FOUND)) return;
            // Allow nothing else
            e.Handled = true;

            btnGuardar.Enabled = true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtApuestaMinima.Text != string.Empty)
            {
                if (actualizarCuotaMinima())
                {
                    MessageBox.Show("Cuota mínima actualizada exitosamente");
                    btnGuardar.Enabled = false;
                }
            }
        }


        public bool actualizarCuotaMinima()
        {
            try
            {
                Credencial credencial = new Credencial().
                                    Select().
                                        Where
                                            (it =>
                                                it.IdCredencial == credencialesId
                                            ).FirstOrDefault();

                credencial.ApuestaMinima = txtApuestaMinima.Text.ToDecimal();
                credencial.Update();
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
