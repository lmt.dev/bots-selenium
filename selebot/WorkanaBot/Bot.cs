﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workana;

namespace WorkanaBot 
{   //prueba logi 2
    //public int Id;
    //public IWebDriver WebDriver;
    public class Bot : DataFirst.Bot
    {
        public List<Proyecto> ExtraerProyectos()
        {
            try
            {
                //este bardo es para scrapear super rapido, se puede hacer mas facil cara de verga
                //HtmlDocument document = new HtmlDocument();
                //string htmlString = WebDriver.PageSource; 
                //document.LoadHtml(htmlString);
                //HtmlNodeCollection collection = document.DocumentNode.SelectNodes("//a");
                //foreach (HtmlNode link in collection)
                //{
                //    string target = link.Attributes["href"].Value;
                //}

                //mas facil, escrapear direacmente del objeto
                try
                {
                    List<Proyecto> ListaProyectos = new List<Proyecto>();
                    string xpathWProyectos = "//div[@id='projects']//div[starts-with(@class, 'project-item')]";
                    List<IWebElement> listaProyectosWE = WebDriver.FindElementsByXPath(xpathWProyectos).ToList();

                    List<ProyectoCaracteristica> listaProyectoCaracteristicas = new ProyectoCaracteristica().Select();


                    foreach (IWebElement wproyecto in listaProyectosWE)
                    {
                        string xpath_titulo = ".//h2[contains(@class,'project-title')]//a//span";
                        string xpath_url = ".//h2[contains(@class,'project-title')]//a";
                        string xpath_presupeustoDisponible = ".//div[@class='project-actions floating']//span[@class='values']";
                        string xpath_fechaPublicado = ".//div[@class='project-body']//span[@class='date']";
                        string xpath_pais = ".//span[@class='country-name']//a";
                        //TODO:scrollear para dibujar los proyectos en la web

                        string titulo = wproyecto.FindElement(By.XPath(xpath_titulo)).GetAttribute("title");
                        string url = wproyecto.FindElement(By.XPath(xpath_url)).GetAttribute("href");
                        string pais = wproyecto.FindElement(By.XPath(xpath_pais)).Text;
                        string presupuestoDisponible = wproyecto.FindElement(By.XPath(xpath_presupeustoDisponible)).Text;
                        string formato = "dd MMMM, yyyy HH:mm";
                        string stringFechaPublicado = wproyecto.FindElement(By.XPath(xpath_fechaPublicado)).GetAttribute("title");
                        string descripcion = wproyecto.FindElement(By.ClassName("project-details")).Text;
                        try
                        {
                            if (descripcion.Contains("Ver más detalles"))
                            {
                                int n = descripcion.IndexOf("Ver más detalles");
                                string z = descripcion.Substring(0,n);

                                int n2 = z.IndexOf("... ");
                                string z2 = descripcion.Substring(0, n2);

                                descripcion = z2;
                            }
                        }
                        catch { }                        
                        DateTime fechaPublicado = new DateTime();
                        try
                        {
                            fechaPublicado = DateTime.ParseExact(stringFechaPublicado, formato, new CultureInfo("es-ES", true));
                        }
                        catch { }

                        Proyecto p = new Proyecto();
                        p.FHAlta = DateTime.Now;
                        p.Titulo = titulo;
                        p.Url = url
                            .Replace("?ref=projects_1", "")
                            .Replace("?ref=projects_2", "")
                            .Replace("?ref=projects_3", "")
                            .Replace("?ref=projects_4", "")
                            .Replace("?ref=projects_5", "")
                            .Replace("?ref=projects_6", "")
                            .Replace("?ref=projects_7", "")
                            .Replace("?ref=projects_8", "")
                            .Replace("?ref=projects_9", "")
                            .Replace("?ref=projects_10", "")
                            .Replace("?ref=projects_11", "")
                            .Replace("?ref=projects_12", "")
                            .Replace("?ref=projects_13", "")
                            .Replace("?ref=projects_14", "")
                            .Replace("?ref=projects_15", "")
                            .Replace("?ref=projects_16", "")
                            .Replace("?ref=projects_17", "")
                            .Replace("?ref=projects_18", "")
                            .Replace("?ref=projects_19", "")
                            .Replace("?ref=projects_20", "")
                            .Replace("?ref=projects_21", "")
                            .Replace("?ref=projects_22", "")
                            .Replace("?ref=projects_23", "")
                            .Replace("?ref=projects_24", "")
                            .Replace("?ref=projects_25", "")
                            .Replace("?ref=projects_26", "")
                            .Replace("?ref=projects_27", "")
                            .Replace("?ref=projects_28", "")
                            .Replace("?ref=projects_29", "")
                            .Replace("?ref=projects_30", "")
                            ;
                        p.Presupuesto = presupuestoDisponible;
                        p.FechaPublicado = fechaPublicado;
                        p.Pais = pais;

                        //Rankear proyecto en nuestra bd
                        p.Puntos = -5;
                        p.Detalles = descripcion;
                         
                        try
                        {
                            foreach (ProyectoCaracteristica caracteristica in listaProyectoCaracteristicas)
                            {
                                char[] separador = { ',' };
                                string[] listaCaracteristicas = caracteristica.Tags.Split(separador);

                                try
                                {
                                    foreach (String caract in listaCaracteristicas)
                                    {
                                        if (wproyecto.Text.ToLower().Contains(caract.ToLower()))
                                        {
                                            p.Puntos = p.Puntos + caracteristica.Puntos;
                                        }
                                    }
                                }
                                catch { }
                                
                            }
                            if (p.Puntos > -100)
                            {
                                ListaProyectos.Add(p);
                                Console.WriteLine("Enviando proyecto a analizar: "+p.ToJson().ToString());
                            }
                            else
                            {
                                Console.WriteLine("Proyecto muy por debajo del puntaje esperado: "+p.ToJson().ToString());
                            }
                            Console.WriteLine("--------------------------------------------------------------------------");
                        }
                        catch
                        { }
                        
                    }
                    return ListaProyectos;
                }
                catch
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }
    }
}
