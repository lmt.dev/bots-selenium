﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using Workana;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MagicSQL;
using DataFirst;
using System.Configuration;

namespace WorkanaBot
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declaracion de core's de procesamiento
            Bot core1 = new Bot();core1.Id = 1;core1.perfilNavegador = "WorkanaBot";
            core1.InicializarWebdriver();

            while (true)
            {
                //Buscamos un origen de datos para filtrar proyectos
                Origen origenCore1 = new Origen().Select().OrderBy(it=>it.FHUltimaLectura).FirstOrDefault();
                core1.logInfo("Origen uilizado: "+origenCore1.Nombre);

                List<Proyecto> listaProyectosEnBD = new Proyecto().Select();

                if (origenCore1 != null)
                {
                    origenCore1.FHUltimaLectura = DateTime.Now;
                    origenCore1.Update();//Se modifica la fecha de utilización del origen

                    core1.IrA(origenCore1.Url);//Se extraen los proyectos de la url del origen consultado
                    List<Proyecto> proyectos = core1.ExtraerProyectos();

                    List<Proyecto> listaProyectosModelo = new Proyecto().Select().Where(r=>r.ModeloTitulo == true || r.ModeloDetalles == true).ToList();

                    foreach (Proyecto proyectoAgregar in proyectos)
                    {
                        bool existe = false;
                        Proyecto proyectoConsulta = listaProyectosEnBD.
                            Where(
                            it =>
                                (it.Url.Contains(proyectoAgregar.Url))
                            ||
                                (
                                    it.Titulo.Contains(proyectoAgregar.Titulo)
                                    &&
                                    it.Presupuesto.Contains(proyectoAgregar.Presupuesto)
                                    &&
                                    it.Pais.Contains(proyectoAgregar.Pais)
                                )
                            ).FirstOrDefault();                        

                        if (proyectoConsulta != null)
                        {
                            existe = true;
                        }

                        if (!existe)
                        {
                            Console.WriteLine("Analizando proyecto: "+proyectoAgregar.ToJson().ToString());
                            foreach (Proyecto proyectoModelo in listaProyectosModelo)
                            {
                                if (proyectoModelo.ModeloTitulo == true)
                                {
                                    int distanciaTitulo = Levenshtein(proyectoModelo.Titulo, proyectoAgregar.Titulo);
                                    if (distanciaTitulo < proyectoModelo.Titulo.Length / 5)
                                    {
                                        proyectoAgregar.Puntos = proyectoAgregar.Puntos + 50;
                                        proyectoAgregar.UltimoEstado = "Titulo parecido a modelo id "+proyectoModelo.IdProyecto.ToString()+";"+proyectoAgregar.UltimoEstado;
                                        Console.WriteLine("Titulo parecido a modelo id " + proyectoModelo.IdProyecto.ToString());
                                        break;
                                    }
                                }

                                if (proyectoModelo.ModeloDetalles == true)
                                {
                                    int distanciaDetalles = Levenshtein(proyectoModelo.Detalles, proyectoAgregar.Detalles);
                                    if (distanciaDetalles < proyectoModelo.Detalles.Length / 3)
                                    {
                                        proyectoAgregar.Puntos = proyectoAgregar.Puntos + 50;
                                        proyectoAgregar.UltimoEstado = "Detalles parecido a modelo id " + proyectoModelo.IdProyecto.ToString() + ";" + proyectoAgregar.UltimoEstado;
                                        Console.WriteLine("Detalles parecido a modelo id " + proyectoModelo.IdProyecto.ToString());
                                        break;
                                    }
                                }
                                continue;
                            }

                            proyectoAgregar.Insert();
                        }
                        continue;
                    }

                    List<Proyecto> listaProyectosPostular =
                        new Proyecto().
                            Select().
                                Where(
                                    it =>
                                    it.Puntos > ConfigurationManager.AppSettings.Get("Puntaje").ToString().ToInt()
                                    &&
                                    it.Postulado == null
                            ).ToList();

                    if (listaProyectosPostular.Count > 0)
                    {
                        core1.logInfo(": Postulando en proyectos");
                        foreach (Proyecto proyectoRecorrer in listaProyectosPostular)
                        {
                            try
                            {
                                core1.IrA
                                    (
                                        proyectoRecorrer.Url.Replace("https://www.workana.com/job/", "https://www.workana.com/messages/bid/")
                                            .Replace("?ref=projects_1", "")
                                            .Replace("?ref=projects_2", "")
                                            .Replace("?ref=projects_3", "")
                                            .Replace("?ref=projects_4", "")
                                            .Replace("?ref=projects_5", "")
                                            .Replace("?ref=projects_6", "")
                                            .Replace("?ref=projects_7", "")
                                            .Replace("?ref=projects_8", "")
                                            .Replace("?ref=projects_9", "")
                                            .Replace("?ref=projects_10", "")
                                            .Replace("?ref=projects_11", "")
                                            .Replace("?ref=projects_12", "")
                                            .Replace("?ref=projects_13", "")
                                            .Replace("?ref=projects_14", "")
                                            .Replace("?ref=projects_15", "")
                                            .Replace("?ref=projects_16", "")
                                            .Replace("?ref=projects_17", "")
                                            .Replace("?ref=projects_18", "")
                                            .Replace("?ref=projects_19", "")
                                            .Replace("?ref=projects_20", "")
                                            .Replace("?ref=projects_21", "")
                                            .Replace("?ref=projects_22", "")
                                            .Replace("?ref=projects_23", "")
                                            .Replace("?ref=projects_24", "")
                                            .Replace("?ref=projects_25", "")
                                            .Replace("?ref=projects_26", "")
                                            .Replace("?ref=projects_27", "")
                                            .Replace("?ref=projects_28", "")
                                            .Replace("?ref=projects_29", "")
                                            .Replace("?ref=projects_30", "")
                                        +
                                        "/florencia-mattello?tab=message"
                                    );
                                Thread.Sleep(10000);
                                try
                                {
                                    //SETEAR SPEECH
                                    //OpenQA.Selenium.IWebElement txtSpeech =
                                    //core1.
                                    //    WebDriver.
                                    //        FindElement
                                    //            (
                                    //        By.XPath(
                                    //            "/html/body/div/div/div[2]/div[6]/div[3]/form/span/div[2]/div[1]/div[1]/div[1]/span/textarea"
                                    //            )
                                    //        );
                                    //txtSpeech.Clear();
                                    //txtSpeech.SendKeys(vSpeechPostulacion);

                                    core1.WebDriver.FindElementByXPath(".//input[@value='Enviar' and @type='submit']").Click();
                                    core1.logInfo(": Postulacion exitosa " + proyectoRecorrer.Titulo);
                                    proyectoRecorrer.Postulado = true;
                                }
                                catch (Exception ex)
                                {
                                    core1.logInfo(": Postulación fallida " + proyectoRecorrer.Titulo);
                                    proyectoRecorrer.UltimoEstado = "Postulación fallida. Reintentar manualmente;" + proyectoRecorrer.UltimoEstado;
                                    proyectoRecorrer.Postulado = false;
                                }
                                //_ProduccionBot.WebDriver.FindElementByXPath("//[@id='SubmitMessage']").Click();                            
                                proyectoRecorrer.Update();
                            }
                            catch (Exception ex)
                            {
                                proyectoRecorrer.Postulado = false;

                                Error err = new Error();
                                err.Ingresar("Error intentando enviar una consulta", ex, "Workana");
                            }
                        }
                    }
                }                
                // FIN de "Mientras" de WorkanaBot
            }
        }
        static int Levenshtein(string s1, string s2)
        {
            int coste = 0
           ; int n1 = s1.Length
           ; int n2 = s2.Length
           ; int[,] m = new int[n1 + 1, n2 + 1];
            for (int i = 0; i <= n1; i++)
                m[i, 0] = i;
            for (int i = 1; i <= n2; i++)
                m[0, i] = i; for (int i1 = 1; i1 <= n1; i1++)
                for (int i2 = 1; i2 <= n2; i2++)
                {
                    coste = (s1[i1 - 1] == s2[i2 - 1]) ? 0 : 1;
                    m[i1, i2] = Math.Min(
                    Math.Min(
                    m[i1 - 1, i2] + 1,
                    m[i1, i2 - 1] + 1),

                    m[i1 - 1, i2 - 1] + coste);
                }
            return m[n1, n2];
        }
        private static string
    vSpeechPostulacion_bak = "";
//@"Buenos días, en caso de que se encuentre interesado en una DEMO por favor indiqueme un día y horario de la franja horaria de su país, en el que se encuentre disponible";

    }
    
}
