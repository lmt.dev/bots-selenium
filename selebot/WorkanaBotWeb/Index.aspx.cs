﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Code;
using MagicSQL;
using static Code.Utils;

namespace WorkanaBotWeb
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                refrescar();
            }
        }
        protected void refrescar()
        {
            List<Workana.Proyecto> listaProyectos = new Workana.Proyecto()
                    .Select()
                        .Where(w=>
                            w.Postulado == null
                            ||                            
                            w.UltimoEstado != null
                            )
                            .OrderByDescending(or2 => or2.FHAlta)
                                .OrderByDescending(or => or.Puntos)                            
                                .ToList();

            gvProyectos.DataSource = listaProyectos.ToDataTable();
            gvProyectos.DataBind();
            upGridViewProyectos.Update();
        }
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            refrescar();
        }

        protected void gvProyectos_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            try
            {
                string vIdProyectoPostulado = gvProyectos.Rows[e.NewSelectedIndex].Cells[0].Text;
                Workana.Proyecto proyecto = new Workana.Proyecto().Select().Where(w=>w.IdProyecto == vIdProyectoPostulado.ToInt()).FirstOrDefault();
                if (proyecto != null)
                {
                    proyecto.Postulado = true;
                    proyecto.UltimoEstado = null;
                    proyecto.Update();                    
                }
            }
            catch (Exception ex)
            {
                Message("Error intentando marcar como postulado", ex.Message, MessagesTypes.error);
            }
            finally
            {
                refrescar();
                Message("Proyecto marcado como postulado", "EXITO", MessagesTypes.success);
            }
        }

        protected void gvProyectos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                string vIdProyectoPostulado = gvProyectos.Rows[e.RowIndex].Cells[0].Text;
                Workana.Proyecto proyecto = new Workana.Proyecto().Select().Where(w => w.IdProyecto == vIdProyectoPostulado.ToInt()).FirstOrDefault();
                if (proyecto != null)
                {
                    proyecto.Postulado = false;
                    proyecto.UltimoEstado = null;
                    proyecto.Update();
                }
            }
            catch (Exception ex)
            {
                Message("Error intentando marcar como descartado", ex.Message, MessagesTypes.error);
            }
            finally
            {
                refrescar();
                Message("Proyecto marcado como descartado", "EXITO", MessagesTypes.success);
            }
        }
    }
}