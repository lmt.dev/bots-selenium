﻿
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CLV;
using DataFirst;
using MagicSQL;

namespace CLVBot
{
    class Program
    {
        static void Main(string[] args)
        {
            Bot _ProduccionBot = new Bot();
            _ProduccionBot.Id = 1;
            _ProduccionBot.perfilNavegador = "ProduccionBot";
            _ProduccionBot.InicializarWebdriver();

            Cuenta cuenta = new Cuenta().Select().FirstOrDefault();

            try
            {
                _ProduccionBot.IrA("https://clasificados.lavoz.com.ar/usuario/login");
                //IWebElement btnLoginWE = _ProduccionBot.WebDriver.FindElementByXPath("//a[@class='ingresar-btn']"); //boton ingresar
                //btnLoginWE.Click();
                Thread.Sleep(2000); //esperar un ratito para q dibuje un popup 
                IWebElement inputEmailWE = _ProduccionBot.WebDriver.FindElement(By.Name("email")); //campo email del popup del login
                inputEmailWE.SendKeys(cuenta.Usuario);
                Thread.Sleep(500); //verificar si estos sleep hacen falta, antes era recomendable luego de los "sendkeys" (enviar teclas al web element)
                IWebElement inputpassWE = _ProduccionBot.WebDriver.FindElement(By.Name("password")); //campo contraseña del popup del login
                inputpassWE.SendKeys(cuenta.Password);
                Thread.Sleep(500);
                IWebElement btnEntrarWE = _ProduccionBot.WebDriver.FindElement(By.CssSelector("input[type='submit']")); // boton iniciar sesion
                btnEntrarWE.Click();
                Thread.Sleep(5000); //esperar q se cargue la pagina con sesion iniciada
                //IWebElement stringUsuarioLogeadoWE = _ProduccionBot.WebDriver.FindElementByXPath("//div[@class='item-list UsuarioLogeado']//div[1]//a"); //eqitueta <a> que tiene de text el nombre de usuario
                //string strUsuario = stringUsuarioLogeadoWE.Text; //string nombre de usuario logeado
                //_ProduccionBot.logInfo("Usuario conectado " + stringUsuarioLogeadoWE.Text);
            }
            catch (Exception e)
            { //el boton de login no está, entonces verificar si al sesion está iniciada
                IWebElement stringUsuarioLogeadoWE = _ProduccionBot.WebDriver.FindElementByXPath("//div[@class='item-list UsuarioLogeado']//div[1]//a"); //eqitueta <a> que tiene de text el nombre de usuario
                string strUsuario = stringUsuarioLogeadoWE.Text;
                //aca hay q verificar si el usuario iniciado es el q nos interesa, sino hay q cerrar la sesion
                bool cerrarSesion = false;  //dejo el codigo para cerrar la sesion
                if (cerrarSesion)
                {
                    _ProduccionBot.IrA("https://adminclasificados.lavoz.com.ar/logout"); //cerrar sesion
                    Thread.Sleep(500);
                    //y aca ya se puede iniciar sesion desde el principio
                    //
                }
            }
            
            while (true) 
            {
                if (
                    DateTime.Now.Hour == 8
                    ||
                    DateTime.Now.Hour == 9
                    ||
                    DateTime.Now.Hour == 10
                    ||
                    DateTime.Now.Hour == 13
                    ||
                    DateTime.Now.Hour == 14
                    ||
                    DateTime.Now.Hour == 15
                    //||
                    //DateTime.Now.Hour == 19
                    //||
                    //DateTime.Now.Hour == 20
                    //||
                    //DateTime.Now.Hour == 21
                    )
                {
                    // Inicio de "Mientras" de Club la voz
                    _ProduccionBot.logInfo("Inicio de ciclo en pagina");
                    int recomenzar = 5;
                    while (recomenzar > 0)
                    {
                        //levantar los avisos de la cuenta.

                        _ProduccionBot.IrA("https://adminclasificados.lavoz.com.ar/administrar/mis-avisos.html");
                        Thread.Sleep(5000);
                        recomenzar = recomenzar - 1;
                        while (true) //loop para recorrrer todas las paginas de avisos del usuario
                        {
                            _ProduccionBot.EsperarPor("//ul[@class='pager']", SCROLLDOWN: 5);

                            ReadOnlyCollection<IWebElement> avisoListWE = _ProduccionBot.WebDriver.FindElementsByXPath("//div[@class='ListadoAdministrador']//div[contains(@class,'AvisoItem AvisoItem-')]"); //obtener los avisos

                            List<Aviso> listaMisAvisos = new List<Aviso>();

                            foreach (IWebElement avisoWE in avisoListWE)
                            {
                                try
                                {
                                    _ProduccionBot.ScrollToElement(avisoWE);
                                }
                                catch
                                {

                                }
                                try
                                {
                                    //string path = "//div[@class='Descripcion']";
                                    Aviso avisoLeido = new Aviso();
                                    avisoLeido.Titulo = avisoWE.FindElement(By.XPath(".//div[contains(@class,'BoxResultado Borde')]//div[@class='Info clearfix']//div[@class='Modelo']//a")).Text;
                                    avisoLeido.NroAviso = avisoWE.FindElement(By.XPath(".//div[contains(@class,'BoxResultado Borde')]//div[@class='Info clearfix']//div[@class='DescripcionLeft']")).Text.Replace(" ", "").Replace("  ", "").Replace("\n", "").Replace(" ", "").Split(':')[1];
                                    string detalles = avisoWE.FindElement(By.XPath(".//div[contains(@class,'BoxResultado Borde')]//div[@class='Info clearfix']//div[@class='Descripcion']")).Text.Replace("\r", "");

                                    detalles = detalles.Replace(": ", "_");
                                    avisoLeido.Estado = detalles.Split('\n')[0].Split('_')[1];
                                    string fpublicado = detalles.Split('\n')[1].Split('_')[1];
                                    string fpiblicadofin = detalles.Split('\n')[2].Split('_')[1];
                                    avisoLeido.Destaque = detalles.Split('\n')[3].Split('_')[1];
                                    avisoLeido.Visitas = detalles.Split('\n')[4].Split('_')[1];
                                    avisoLeido.Contactos = detalles.Split('\n')[5].Split('_')[1];

                                    avisoLeido.FHPublicado = fpublicado;
                                    avisoLeido.FHPublicadoFin = fpiblicadofin;

                                    Aviso avisoComparar = new Aviso().
                                            Select().
                                                Where(
                                                    it =>
                                                        it.NroAviso == avisoLeido.NroAviso
                                                        ).FirstOrDefault();

                                    if (avisoComparar == null)
                                    {
                                        avisoLeido.FHAlta = DateTime.Now;
                                        avisoLeido.IdCuenta = cuenta.IdCuenta;
                                        avisoLeido.FHUltimaPublicacion = DateTime.Now;
                                        avisoLeido.Insert();
                                        _ProduccionBot.logInfo("Insertando nuevo aviso " + avisoLeido.Titulo);
                                    }
                                    else
                                    {
                                        avisoLeido.IdAviso = avisoComparar.IdAviso;
                                        avisoLeido.FHUltimaPublicacion = avisoComparar.FHUltimaPublicacion;
                                        avisoComparar.Delete();
                                        avisoLeido.Insert();
                                        _ProduccionBot.logInfo("Aviso actualizado " + avisoLeido.Titulo);
                                    }

                                    if (avisoLeido.FHUltimaPublicacion < DateTime.Now.AddHours(-3))
                                    {
                                        switch (avisoLeido.Estado)
                                        {
                                            case "Publicado":
                                                _ProduccionBot.logInfo("Despublicando aviso " + avisoLeido.Titulo);
                                                DespublicarAviso(_ProduccionBot);
                                                break;

                                            case "Cancelado":
                                                _ProduccionBot.logInfo("Publicando aviso " + avisoLeido.Titulo);
                                                PublicarAviso(_ProduccionBot, avisoLeido.Destaque);
                                                avisoLeido.Estado = "Publicado";
                                                avisoLeido.FHUltimaPublicacion = DateTime.Now;
                                                avisoLeido.Update();
                                                break;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Error vError = new Error();
                                    vError.Ingresar("Error intentando listar mis avisos", ex, "CLV");
                                    CerrarModalCLV(_ProduccionBot);
                                    break;
                                }
                            }

                            try
                            {
                                _ProduccionBot.WebDriver.FindElementByXPath("//a[@title='Ir a la página siguiente']").Click(); //intentar click en "pagina siguiente" del apginador
                                Thread.Sleep(3000);
                            }
                            catch
                            {
                                break; //si falla es porque no hay mas paginas para revisar.
                            }
                        }

                    }

                    _ProduccionBot.logInfo("Fin de ciclo en pagina");
                    Thread.Sleep(10000);
                    // Fin de "Mientras" de Club la voz
                }
                else
                {
                    _ProduccionBot.logInfo("Aguardando 10 minutos...");
                    Thread.Sleep(600000);
                }
            }
        }

        private static void DespublicarAviso(DataFirst.Bot bot)
        {
            //son 2 botones q hay q apretar para despublicar
            while (true)
            {
                try
                {
                    bot.WebDriver.FindElementByXPath(".//a[@title='Despublicar el Aviso']").Click();
                    bot.logInfo("Despublicacion realizada seccion 1");
                    Thread.Sleep(1000);
                    break;
                }
                catch (Exception ex)
                {
                    Thread.Sleep(1000);
                    bot.logInfo("Error intentando despublicar seccion 1" + ex.Message);
                    bot.IrA("https://adminclasificados.lavoz.com.ar/administrar/mis-avisos.html");
                    CerrarModalCLV(bot);
                }
            }
            while (true)
            {
                try
                {
                    bot.WebDriver.FindElementByXPath(".//input[@value='Despublicar']").Click();
                    bot.logInfo("Despublicacion realizada seccion 2");
                    Thread.Sleep(1000);
                    break;
                }
                catch (Exception ex)
                {
                    Thread.Sleep(1000);
                    bot.logEx("Error intentando despublicar seccion 2", ex, "CLVBot");
                    bot.IrA("https://adminclasificados.lavoz.com.ar/administrar/mis-avisos.html");
                    CerrarModalCLV(bot);
                }
            }
            bot.EsperarPor(".//button[@id='cboxClose']", CLICKEAR: true, REINTENTOS: 5);
            //CerrarModalCLV(bot);
        }

        private static void PublicarAviso(DataFirst.Bot bot, string plan)
        {
            //bot.EsperarPor(".//a[@title='Publicar el Aviso']",3,10,3,true);
            bot.EsperarPor(".//a[@title='Publicar el Aviso']", CLICKEAR: true);

            bot.logInfo("Seleccionando categoria de aviso");

            var radioButtonID = "";

            switch (plan)
            {
                case "Super Premium":
                    radioButtonID = "edit-espacios-1009";
                    break;
                case "Premium":
                    radioButtonID = "edit-espacios-8";
                    break;
                case "Estandar":
                    radioButtonID = "edit-espacios-7";
                    break;
                case "Basico":
                    radioButtonID = "edit-espacios-6";
                    break;
                case "Vidriera":
                    radioButtonID = "edit-espacios-308834";
                    break;
                case "Gratuito":
                    radioButtonID = "edit-espacios-5";
                    break;
            }

            while (true)
            {
                try
                {
                    //bot.WebDriver.FindElementByXPath(".//input[@id='" + radioButtonID + "']").Click();
                    bot.EsperarPor(".//input[@id='" + radioButtonID + "']", CLICKEAR: true);

                    //probablemente para este boton haya q hacer scroll down (para es es el objeto web driver) o tener el zoom en 60% o algo asi
                    bot.WebDriver.ExecuteScript("window.scrollBy(0,600)");

                    //Confirmar
                    bot.EsperarPor(".//input[@value='Confirmar']", CLICKEAR: true);

                    break;
                }
                catch
                {
                    CerrarModalCLV(bot);
                }
            }
            bot.EsperarPor(".//button[@id='cboxClose']", CLICKEAR: true, REINTENTOS: 5);
            //CerrarModalCLV(bot);
        }

        public static void CerrarModalCLV(DataFirst.Bot bot)
        {
            try
            {
                bot.logInfo("Cerrando modal");
                bot.WebDriver.FindElementById("cboxClose").Click();
                bot.logInfo("Modal cerrado");
                Thread.Sleep(1000);
            }
            catch
            {
                bot.logInfo("Modal no encontrado");
            }
        }

    }
}
