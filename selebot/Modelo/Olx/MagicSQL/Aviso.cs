﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace Olx
{
    public partial class Aviso : ISUD<Aviso>
    {
        public Aviso() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdAviso { get; set; }

        public DateTime? FHAlta { get; set; }

        public string Url { get; set; }

        public DateTime? FHMensajeEnviado { get; set; }

        public string Titulo { get; set; }
    }
}