﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Bet365
{
    public partial class Equipo
    {

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int PuntosAproximacion { get; set; }
        public int CantidadAproximaciones { get; set; }
    }
}
