﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace Bet365
{
    public partial class Proxy : ISUD<Proxy>
    {
        public Proxy() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdProxy { get; set; }

        public DateTime? FHAlta { get; set; }

        public string IP { get; set; }

        public string Puerto { get; set; }

        public string Usuario { get; set; }

        public string Password { get; set; }

        public int? IdPais { get; set; }
    }
}