﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace Bet365
{
    public partial class Credencial : ISUD<Credencial>
    {
        public Credencial() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdCredencial { get; set; }

        public DateTime? FhAlta { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string Usuario { get; set; }

        public string Password { get; set; }

        public DateTime? FHBaja { get; set; }

        public decimal? ApuestaMinima { get; set; }
    }
}