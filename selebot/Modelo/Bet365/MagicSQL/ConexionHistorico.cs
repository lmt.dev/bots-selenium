﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace Bet365
{
    public partial class ConexionHistorico : ISUD<ConexionHistorico>
    {
        public ConexionHistorico() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdConexionHistorico { get; set; }

        public int IdConexion { get; set; }

        public DateTime? FHAlta { get; set; }

        public int? IdCredencial { get; set; }

        public int? IdCuenta { get; set; }

        public int? IdProxy { get; set; }

        public int? IdPais { get; set; }
    }
}