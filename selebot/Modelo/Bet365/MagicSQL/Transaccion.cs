﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace Bet365
{
    public partial class Transaccion : ISUD<Transaccion>
    {
        public Transaccion() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdTransaccion { get; set; }

        public DateTime? FHAlta { get; set; }

        public string Descripcion { get; set; }

        public int? IdConexion { get; set; }
    }
}