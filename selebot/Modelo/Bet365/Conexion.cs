﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]
// IMPORTANT:
//           Custom Properties have to start with "_". Example: public int _MyProperty { get; set; }
//           Fields can start without "_". Example: public int MyField;

using MagicSQL;
using System.Collections.Generic;

namespace Bet365
{
    public partial class Conexion
    {
        // Models

        public Credencial Credencial;
        public Cuenta Cuenta;
        public Pais Pais;
        public Proxy Proxy;

        // Lists

        public List<ConexionHistorico> ListConexionHistorico_Conexion;
        public List<Transaccion> ListTransaccion_Conexion;
    }
}