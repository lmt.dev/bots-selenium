﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]
// IMPORTANT:
//           Custom Properties have to start with "_". Example: public int _MyProperty { get; set; }
//           Fields can start without "_". Example: public int MyField;

using MagicSQL;
using System.Collections.Generic;

namespace Bet365
{
    public partial class Pais
    {
        // Models


        // Lists

        public List<Conexion> ListConexion_Pais;
        public List<Cuenta> ListCuenta_Pais;
        public List<Proxy> ListProxy_Pais;
    }
}