﻿using MagicSQL;
using System;
using System.Collections.Generic;

namespace Bet365
{
    public partial class Apuesta
    {

        // Models

        public Cuenta Cuenta;

        public string Estrategia { get; set; }
        public bool Finalizada { get; set; }


        // Lists
        public string Nombre()
        {
            return Local + " v " + Visitante + " /" + Estrategia;
        }

        public string CANTIDAD_CORNERS { get; set; }
        public string CANTIDAD_CORNERS2 { get; set; }

        public string MinutosCancelar { get; set; }
        public string MinutosCancelar2 { get; set; }
        public bool SubApuesta1Finalizada { get; set; }
        public bool SubApuesta2Finalizada { get; set; }


        public string MercadoString { get; set; }
        public bool Basura { get; set; }
        public int CornersLocal { get; set; }
        public int CornersVisitante { get; set; }
        public int CornersVisitantelPrimeraLectura { get; set; }
        public int CornersLocalPrimeraLectura { get; set; }

        public Apuesta Clonar()
        {
            Apuesta apuestanew = new Apuesta();
            apuestanew.Local = Local;
            apuestanew.Visitante = Visitante;
            apuestanew.Estrategia = Estrategia;
            apuestanew.Monto = Monto;
            apuestanew.CuotaMinima = CuotaMinima;
            apuestanew.MercadoString = MercadoString;
            return apuestanew;
        }

        public bool VerificarCorners()
        {
            //bool result = 
            //this.Basura = !result;
            return CornersLocal.Equals(CornersLocalPrimeraLectura) && CornersVisitante.Equals(CornersVisitantelPrimeraLectura); ;
        }

        public DateTime horaInicioEjecucion { get; set; }

        public string Fase { get; set; }
        public int Current { get; set; }
    }
}