﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace Workana
{
    public partial class ProyectoCaracteristica : ISUD<ProyectoCaracteristica>
    {
        public ProyectoCaracteristica() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdProyectoCaracteristica { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public string Tags { get; set; }

        public int? Puntos { get; set; }
    }
}