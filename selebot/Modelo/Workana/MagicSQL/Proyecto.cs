﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace Workana
{
    public partial class Proyecto : ISUD<Proyecto>
    {
        public Proyecto() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdProyecto { get; set; }

        public DateTime? FHAlta { get; set; }

        public string Titulo { get; set; }

        public string Url { get; set; }

        public string Presupuesto { get; set; }

        public string Pais { get; set; }

        public DateTime? FechaPublicado { get; set; }

        public string Detalles { get; set; }

        public string Tags { get; set; }

        public int? Puntos { get; set; }

        public bool? Postulado { get; set; }

        public string UltimoEstado { get; set; }

        public bool? ModeloTitulo { get; set; }

        public bool? ModeloDetalles { get; set; }
    }
}