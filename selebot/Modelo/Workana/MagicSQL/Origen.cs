﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace Workana
{
    public partial class Origen : ISUD<Origen>
    {
        public Origen() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdOrigen { get; set; }

        public string Nombre { get; set; }

        public string Url { get; set; }

        public bool? Activo { get; set; }

        public DateTime? FHUltimaLectura { get; set; }
    }
}