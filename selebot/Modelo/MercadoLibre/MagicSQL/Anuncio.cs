﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace MercadoLibre
{
    public partial class Anuncio : ISUD<Anuncio>
    {
        public Anuncio() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdAnuncio { get; set; }

        public DateTime? FHAlta { get; set; }

        public string Descripcion { get; set; }

        public string Url { get; set; }

        public string Nombre { get; set; }

        public string Teléfono { get; set; }
    }
}