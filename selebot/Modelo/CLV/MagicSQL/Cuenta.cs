﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace CLV
{
    public partial class Cuenta : ISUD<Cuenta>
    {
        public Cuenta() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdCuenta { get; set; }

        public DateTime? FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }

        public string Usuario { get; set; }

        public string Password { get; set; }
    }
}