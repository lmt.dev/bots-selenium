﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace CLV
{
    public partial class Aviso : ISUD<Aviso>
    {
        public Aviso() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdAviso { get; set; }

        public DateTime? FHAlta { get; set; }

        public DateTime? FHBaja { get; set; }

        public string Titulo { get; set; }

        public string NroAviso { get; set; }

        public string Estado { get; set; }

        public string FHPublicado { get; set; }

        public string FHPublicadoFin { get; set; }

        public string Destaque { get; set; }

        public string Visitas { get; set; }

        public string Contactos { get; set; }

        public int? IdCuenta { get; set; }

        public DateTime? FHUltimaPublicacion { get; set; }
    }
}