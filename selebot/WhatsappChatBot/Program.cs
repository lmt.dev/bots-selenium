﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DataFirst;
using MagicSQL;

namespace WhatsappChatBot
{
    class Program
    {
        static void Main(string[] args)
        {
            iniciarNavegadores();
            while (true)
            {
                ReadOnlyCollection<IWebElement> conversacionesList = _WhatsAppChatBot.WebDriver.FindElementsByXPath("//div[@class='_21sW0 _1ecJY']//div[contains(@class,'_2wP_Y')]");
                foreach (IWebElement conversacionIE in conversacionesList)
                {
                    try
                    {                        
                        string[] conversacion = conversacionIE.Text.Split('\n');                        
                        string vGrupo = "";
                        string vContacto = "";
                        string vUltimoMensaje = "";
                        string vHoraUltimoMensaje = "";

                        switch (conversacion.Count())
                        {
                            case 10:
                            case 8:
                            case 7:
                            case 6:
                            case 5:
                                vGrupo = conversacion[0];
                                vHoraUltimoMensaje = conversacion[1];
                                vContacto = conversacion[2];
                                vUltimoMensaje = conversacion[3]+Environment.NewLine+ conversacion[4];
                                break;

                            case 4:
                                vGrupo = conversacion[0];
                                vHoraUltimoMensaje = conversacion[1];
                                vContacto = conversacion[2];
                                vUltimoMensaje = conversacion[3];
                                break;

                            case 3:
                                vContacto = conversacion[0];
                                vHoraUltimoMensaje = conversacion[1];
                                vUltimoMensaje = conversacion[2];
                                break;

                            case 2:
                                vContacto = conversacion[0];
                                vUltimoMensaje = conversacion[1];
                                break;

                        }

                        if (!vGrupo.Equals(""))
                        {
                            Console.WriteLine("vGrupo: " + vGrupo);
                        }
                        Console.WriteLine("vContacto: "+ vContacto);
                        Console.WriteLine("vHoraUltimoMensaje: " + vHoraUltimoMensaje);
                        Console.WriteLine("vUltimoMensaje: " + vUltimoMensaje);
                        Console.WriteLine("--------------------------------------");
                    }
                    catch
                    {
                    }
                }
            }
        }
        private static Bot _WhatsAppChatBot { get; set; }
        private static void iniciarNavegadores()
        {
            _WhatsAppChatBot = new Bot();
            _WhatsAppChatBot.Id = 1;
            _WhatsAppChatBot.InicializarWebdriver();
            _WhatsAppChatBot.IrA("https://web.whatsapp.com/");
            Thread.Sleep(5000);
            Console.WriteLine("Inicio exitoso a la bandeja de WhatsApp...");
        }
    }
}
