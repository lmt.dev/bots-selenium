﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Bet365;
using Modelo.NATS;
using System.Configuration;
using TelgramBot;

namespace TelegramBot
{

    public class TelegramBot : DataFirst.Bot
    {
        internal NatsHandler natsHandler;

        public TelegramBot()
        {
            natsHandler = new NatsHandler("nats://ec2-3-124-242-27.eu-central-1.compute.amazonaws.com");
        }


        public void IniciarTelegramBot()
        {
//            ApuestasHandler apuestasHandler = new ApuestasHandler();
            this.InicializarWebdriver();
            this.IrA("https://web.telegram.org");
            Thread.Sleep(5000);
            List<string> id_enviados = new List<string>();
            //bool primera_ejecucion = true;

            #region ignorar mensajes viejos
            Thread.Sleep(5000);
            this.EsperarPor(".//ul[@class='nav nav-pills nav-stacked']");
            var contenedor_de_conversaciones = WebDriver.FindElementByXPath(".//ul[@class='nav nav-pills nav-stacked']");
            var conversaciones = contenedor_de_conversaciones.FindElements(By.XPath(".//li[@dialog-message='dialogMessage']"));
            IWebElement conversacion_gallego = WebDriver.FindElementByXPath(".//div");
            foreach (var conversacion in conversaciones)
            {
                var conversacionTitulo = conversacion.FindElement(By.XPath(".//span[@my-peer-link='dialogMessage.peerID']")).Text;
                //if (conversacionTitulo.Equals("Bender Bot Goals"))
                if (conversacionTitulo.Equals(ConfigurationManager.AppSettings.Get("contactoTelegram").ToString()))
                {
                    conversacion_gallego = conversacion;
                    conversacion.GetAttribute("class");
                    conversacion.FindElement(By.XPath(".//a")).Click();
                    Thread.Sleep(5000);
                    conversacion.FindElement(By.XPath(".//a")).Click();
                    break;
                }
            }

            this.EsperarPor(".//div[@class='im_history_messages_peer']");
            var contendor_mensajes = WebDriver.FindElementByXPath(".//div[@class='im_history_messages_peer']");
            Thread.Sleep(1000);
            var mensajes = contendor_mensajes.FindElements(By.XPath(".//div[contains(@class,'im_history_message_wrap')]"));
            foreach (var mensajeWE in mensajes)
            {
                string mensajeId = "0";
                try
                {
                    mensajeId = mensajeWE.FindElement(By.XPath(".//div[2]")).GetAttribute("data-msg-id");
                    if (mensajeId == null)
                    {
                        mensajeId = mensajeWE.FindElement(By.XPath(".//div[1]")).GetAttribute("data-msg-id");
                    }
                }
                catch
                {

                }
                id_enviados.Add(mensajeId);
            }
            #endregion

            while (true)
            {
                this.EsperarPor(".//ul[@class='nav nav-pills nav-stacked']");
                contenedor_de_conversaciones = WebDriver.FindElementByXPath(".//ul[@class='nav nav-pills nav-stacked']");
                conversaciones = contenedor_de_conversaciones.FindElements(By.XPath(".//li[@dialog-message='dialogMessage']"));

                foreach (var conversacion in conversaciones)
                {
                    var conversacionTitulo = conversacion.FindElement(By.XPath(".//span[@my-peer-link='dialogMessage.peerID']")).Text;
                    //if (conversacionTitulo.Equals("Bender Bot Goals"))
                    if (conversacionTitulo.Equals(ConfigurationManager.AppSettings.Get("contactoTelegram").ToString()))
                    {
                        conversacion_gallego = conversacion;
                        conversacion.GetAttribute("class");
                        conversacion.FindElement(By.XPath(".//a")).Click();
                        break;
                    }
                }
                try
                {
                    this.EsperarPor(".//div[@class='im_history_messages_peer']");
                    contendor_mensajes = WebDriver.FindElementByXPath(".//div[@class='im_history_messages_peer']");
                    //Thread.Sleep(1000);
                    mensajes = contendor_mensajes.FindElements(By.XPath(".//div[contains(@class,'im_history_message_wrap')]"));
                    foreach (var mensajeWE in mensajes)
                    {
                        string mensajeId = "";
                        try
                        {
                            mensajeId = mensajeWE.FindElement(By.XPath(".//div[2]")).GetAttribute("data-msg-id");
                            if (mensajeId == null)
                            {
                                mensajeId = mensajeWE.FindElement(By.XPath(".//div[1]")).GetAttribute("data-msg-id");
                            }
                        }
                        catch
                        {
                            continue;
                        }


                        if (!id_enviados.Contains(mensajeId))
                        {
                            id_enviados.Add(mensajeId);
                            if (true)
                            {
                                var mensajeRaw = mensajeWE.FindElement(By.XPath(".//div[@class='im_message_text']")).Text;
                                var resultString = Regex.Replace(mensajeRaw, @"^\s+$[\r\n]*", string.Empty, RegexOptions.Multiline);
                                string[] lineas = resultString.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

                                SeñalInplayScanner so = new SeñalInplayScanner();so.FHAlta = DateTime.Now;
                                var titulo = lineas[0]; so.titulo = titulo;
                                var liga_equipos = lineas[1]; so.liga_equipos = liga_equipos;
                                var liga = liga_equipos.Split('/')[0]; so.liga = liga;
                                var vEquiposAmbos = liga_equipos.Split('/')[1]; so.vEquiposAmbos = vEquiposAmbos;
                                var equipo_local = vEquiposAmbos.Split(new string[] { " v " }, StringSplitOptions.None)[0].Trim(); so.equipo_local = equipo_local;
                                var equipo_visitante = vEquiposAmbos.Split(new string[] { " v " }, StringSplitOptions.None)[1].Trim(); so.equipo_visitante = equipo_visitante;
                                var marcador_local = lineas[2].Replace("Placar:", "").Split('-')[0].Trim();so.marcador_local = marcador_local;
                                var marcador_visitante = lineas[2].Replace("Placar:", "").Split('-')[1].Trim();so.marcador_visitante=marcador_visitante;
                                var tiempo_texto = lineas[3];so.tiempo_texto = tiempo_texto;
                                var tiros_arco_local = lineas[4].Replace("Chutes ao gol:", "").Split('-')[0].Trim();so.tiros_arco_local = tiros_arco_local;
                                var tiros_arco_visitante = lineas[4].Replace("Chutes ao gol:", "").Split('-')[1].Trim();so.tiros_arco_visitante = tiros_arco_visitante;
                                var tiros_fuera_local = lineas[5].Replace("Chutes fora do gol:", "").Split('-')[0].Trim();so.tiros_fuera_local = tiros_fuera_local;
                                var tiros_fuera_visitante = lineas[5].Replace("Chutes fora do gol:", "").Split('-')[1].Trim();so.tiros_fuera_visitante = tiros_fuera_visitante;
                                var corners_local = lineas[6].Replace("Cantos:", "").Split('-')[0].Trim();so.corners_local = corners_local;
                                var corners_visitante = lineas[6].Replace("Cantos:", "").Split('-')[1].Trim();so.corners_visitante = corners_visitante;
                                var posesion_balon_local = lineas[7].Replace("Posse de bola:", "").Split('-')[0].Trim();so.posesion_balon_local = posesion_balon_local;
                                var posesion_balon_visitante = lineas[7].Replace("Posse de bola:", "").Split('-')[1].Trim();so.posesion_balon_visitante = posesion_balon_visitante;
                                var tarjetas_rojas_local = lineas[8].Replace("Cartões vermelhos:", "").Split('-')[0].Trim();so.tarjetas_rojas_local = tarjetas_rojas_local;
                                var tarjetas_rojas_visitante = lineas[8].Replace("Cartões vermelhos:", "").Split('-')[1].Trim();so.tarjetas_rojas_visitante = tarjetas_rojas_visitante;
                                var PI1_local = lineas[9].Replace("PI1:", "").Split('-')[0].Trim();so.PI1_local = PI1_local;
                                var PI1_visitante = lineas[9].Replace("PI1:", "").Split('-')[1].Trim();so.PI1_visitante = PI1_visitante;
                                var PI2_local = lineas[10].Replace("PI2:", "").Split('-')[0].Trim();so.PI2_local = PI2_local;
                                var PI2_visitante = lineas[10].Replace("PI2:", "").Split('-')[1].Trim();so.PI2_visitante = PI2_visitante;
                                so.Insert();

                                Señal señal = new Señal();
                                señal.MensajeRAW = mensajeRaw;
                                señal.MarcadorLocal = marcador_local;
                                señal.EquipoVisitante = marcador_visitante;
                                señal.EquipoLocal = equipo_local;
                                señal.EquipoVisitante = equipo_visitante;
                                señal.CornersLocal = corners_local;
                                señal.CornersVisitante = corners_visitante;
                                señal.PI1Local = PI2_local;
                                señal.PI1Visitante = PI2_visitante;
                                señal.SetEstrategia(titulo);

                                if(señal.Estrategia!=null)
                                    natsHandler.Send(señal);


                            }
                        }
                    }
                    //primera_ejecucion = false;

                }
                catch (Exception e)
                { }
            }



        }

    }
}
