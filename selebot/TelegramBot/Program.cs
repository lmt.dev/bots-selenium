﻿using Modelo.NATS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBot
{
    class Program
    {
        static void Main(string[] args)
        {
            #region parametrizacion
            TelegramBot tb = new TelegramBot();

            #endregion

            #region main thread
            tb.HEADLESS = false;
            tb.IniciarTelegramBot();

            #endregion


        }
    }
}
