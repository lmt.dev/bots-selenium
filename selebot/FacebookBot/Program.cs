﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using Workana;
using System.Threading;
using System.Configuration;
using System.Xml;
using System.IO;
using System.Net;
using System.IO.Compression;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using MagicSQL;
using DataFirst;
using Facebook;

namespace FacebookBot
{
    class Program
    {
        static void Main(string[] args)
        {
            iniciarNavegadores();
            DateTime dtCheckPoint = DateTime.Now.AddMinutes(1);
            while (true)
            {
                if (dtCheckPoint > DateTime.Now)
                {
                    ReadOnlyCollection<IWebElement> postListWE = _FacebookMuro.WebDriver.FindElementsByXPath("//div[contains(@class,'_4ikz')]");
                    foreach (IWebElement post in postListWE)
                    {
                        try
                        {
                            Post postCarga = new Post();
                            IWebElement ie1 = post.FindElement(By.XPath(".//a[@class='_wpv']"));
                            string vGrupo = ie1.Text;
                            postCarga.GrupoNombre = vGrupo;
                            postCarga.GrupoLink = ie1.GetAttribute("href").Substring(0, ie1.GetAttribute("href").IndexOf("?")).Replace("https://www.facebook.com/", "");

                            IWebElement ie2 = post.FindElement(By.XPath(".//span[@class='fwb fcg']"));
                            postCarga.PerfilNombre = ie2.Text.Substring(0, ie2.Text.IndexOf("\n")).Replace("\r", "").Replace("?fref=nf", "").Replace("https://www.facebook.com/", "");

                            IWebElement ie3 = post.FindElement(By.XPath(".//a[@class='_5pb8 d_1antvcdhts _8o _8s lfloat _ohe']"));
                            postCarga.PerfilLink = ie3.GetAttribute("href").Substring(0, ie3.GetAttribute("href").IndexOf("&")).Replace("?fref=nf", "").Replace("https://www.facebook.com/", "");

                            IWebElement ie3b = post.FindElement(By.XPath(".//img[@class='_s0 _4ooo _5xib _5sq7 _44ma _rw img']"));
                            postCarga.PerfilFoto = ie3b.GetAttribute("src");

                            try
                            {
                                IWebElement ie4 = post.FindElement(By.XPath(".//div[@data-testid='post_message']"));
                                postCarga.Contenido = ie4.Text;
                            } catch { }

                            try
                            {                                
                                IWebElement ie4b = post.FindElement(By.XPath(".//img[@class='scaledImageFitWidth img']"));
                                postCarga.ImgAlt = ie4b.GetAttribute("alt");
                                postCarga.ImgSrc = ie4b.GetAttribute("data-src");
                            }
                            catch { }

                            IWebElement ie5 = post.FindElement(By.XPath(".//div[@class='_5pcp _5lel _2jyu _232_']"));
                            postCarga.Permalink = ie5.GetAttribute("id").Split(';')[2];
                            /*Esconden el href por js, se muestra al posicionar el mouse en la hora, pero el js lo saca de ese div*/

                            if (postCarga.GrupoNombre != string.Empty && postCarga.GrupoNombre != null
                                &&
                                postCarga.GrupoLink != string.Empty && postCarga.GrupoLink != null
                                &&
                                postCarga.PerfilNombre != string.Empty && postCarga.PerfilNombre != null
                                &&
                                postCarga.PerfilLink != string.Empty && postCarga.PerfilLink != null
                                &&
                                postCarga.Permalink != string.Empty && postCarga.Permalink != null
                                &&
                                postCarga.PerfilFoto != string.Empty && postCarga.PerfilFoto != null
                                )
                            {
                                agregarPost(postCarga);
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Reiniciando navegador...");
                    _FacebookMuro.WebDriver.Close();
                    _FacebookMuro.WebDriver.Dispose();
                    iniciarNavegadores();
                    dtCheckPoint = DateTime.Now.AddMinutes(1);
                }
                _FacebookMuro.WebDriver.ExecuteScript("window.scrollBy(0,600)");
            }
        }
        private static Bot _FacebookMuro { get; set; }
        public static void agregarPost(Post post)
        {
            post.FHAlta = DateTime.Now;

            if (
                    new Post().Select().Where(
                    it =>
                        it.Permalink == post.Permalink && it.GrupoLink == post.GrupoLink
                    ).FirstOrDefault() == null
                )
            {
                post.Insert();
            }

            Console.WriteLine("Nuevo Post: " + post.ToJson().ToString());
        }
        private static void iniciarNavegadores()
        {
            _FacebookMuro = new Bot();
            _FacebookMuro.Id = 1;
            _FacebookMuro.InicializarWebdriver();
            _FacebookMuro.IrA("https://www.facebook.com/");
            Thread.Sleep(5000);
            Console.WriteLine("Inicio exitoso al muro...");
        }
    }
}
