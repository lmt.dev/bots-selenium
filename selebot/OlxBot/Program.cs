﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Olx;

namespace OlxBot
{
    class Program
    {
        static void Main(string[] args)
        {
            //Inicializa todas las variables hasta que te dejen ir
            setSpeech();
            DataFirst.Bot _ProduccionBot = new DataFirst.Bot();
            _ProduccionBot.InicializarWebdriver();
            Thread.Sleep(5000);
            vContadorEnviados = 0;
            while(vContadorEnviados<301)
            {
                // _ProduccionBot.IrA("https://www.olx.com.ar/autos_c378");
                _ProduccionBot.IrA("https://www.olx.com.ar/departamentos-casas-venta_c367");

                listaAvisos = new List<Aviso>();
                Thread.Sleep(5000);

                vMultiplicadorLista = 1;
                //Expandir anuncios
                expandirAnuncios(_ProduccionBot);

                //Capturar avisos
                capturarAvisos(_ProduccionBot);

                //Procesar avisos
                procesarAvisos(_ProduccionBot);

                //Aguardando una hora...
                Console.WriteLine("Aguardando una hora...");
                Thread.Sleep(3600000);
            }
        }
        
        public static List<Aviso> listaAvisos { get; set; }
        public static int vMultiplicadorLista { get; set; }
        public static int vContadorEnviados { get; set; }
        public static void capturarAvisos(DataFirst.Bot _ProduccionBot)
        {
            int vIdAnuncio = 1;
            int vTotalBuscar = 21 * vMultiplicadorLista;
            while (vIdAnuncio < vTotalBuscar)
            {
                anuncioObtenido(_ProduccionBot, vIdAnuncio);
                vIdAnuncio++;
            }
        }

        public static void procesarAvisos(DataFirst.Bot _ProduccionBot)
        {
            //Listar pendientes
            List<Aviso> listaAvisosPendientes = new List<Aviso>();
            Console.WriteLine("Depurando avisos pendientes...");
            foreach (Aviso avisoRecorrer in listaAvisos)
            {
                Aviso avisoConsultar = new Aviso().Select().Where(it => it.Url == avisoRecorrer.Url).FirstOrDefault();
                if (avisoConsultar == null)
                {
                    listaAvisosPendientes.Add(avisoRecorrer);
                }
            }

            //Recorrer avisos
            foreach (Aviso avisoPostular in listaAvisosPendientes)
            {
                Console.WriteLine("Enviando mensaje...");
                try
                {
                    //Abrir modal de contacto
                    Console.WriteLine("Abrir modal de contacto");
                    _ProduccionBot.IrA(avisoPostular.Url);
                    Thread.Sleep(1000);

                    IWebElement weBotonContactar =
                        _ProduccionBot.
                            WebDriver.
                                FindElement
                                    (
                                By.XPath(
                                    "/html/body/div/div/main/div/div/div[5]/div[2]/div/div/button"
                                    )
                                );
                    weBotonContactar.Click();

                    //Setear celular
                    Console.WriteLine("Setear celular");
                    IWebElement weTxtCelular =
                        _ProduccionBot.
                            WebDriver.
                                FindElement
                                    (
                                By.XPath(
                                    "/html/body/div/div/main/div/div/div[6]/div[2]/div/div[5]/div/div/div[2]/div[1]/input"
                                    )
                                );
                    weTxtCelular.Clear();
                    weTxtCelular.SendKeys("3512753053");

                    //Enviar mensaje
                    Console.WriteLine("Enviar mensaje");
                    _ProduccionBot.EsperarPor(
                        "/html/body/div/div/main/div/div/div[6]/div[2]/div/div[7]/button",
                        2,
                        -1,
                        3,
                        true
                        );

                    //Boton confirmar datos
                    Console.WriteLine("Boton confirmar datos");
                    _ProduccionBot.EsperarPor(
                        "/html/body/div/div/main/div/div/div[6]/div[2]/div/div/div[2]/button[1]",
                        2,
                        -1,
                        3,
                        true
                        );

                    try
                    {
                        //Pasar al chat
                        Console.WriteLine("Pasar al chat");
                        _ProduccionBot.EsperarPor(
                            "/html/body/div/div/main/div/div/div[6]/div[2]/div[2]/button",
                            2,
                            -1,
                            3,
                            true
                            );
                    }
                    catch
                    {

                    }


                    //Nuevo mensaje
                    Console.WriteLine("Nuevo mensaje");
                    IWebElement weTxtNuevoMensaje =
                        _ProduccionBot.
                            WebDriver.
                                FindElement
                                    (
                                By.XPath(
                                    "/html/body/div/div/main/div/div/div[2]/div/div[3]/textarea"
                                    )
                                );
                    weTxtNuevoMensaje.SendKeys(vSpeech);

                    //Enviar nuevo mensaje
                    Console.WriteLine("Enviar nuevo mensaje");
                    _ProduccionBot.EsperarPor(
                        "/html/body/div/div/main/div/div/div[2]/div/div[3]/span",
                        2,
                        -1,
                        3,
                        true
                        );

                    avisoPostular.FHMensajeEnviado = DateTime.Now;
                    avisoPostular.Insert();
                    vContadorEnviados++;
                    Thread.Sleep(5000);
                }
                catch
                {
                    Console.WriteLine("Error postulando aviso");
                    avisoPostular.Insert();
                }
            }
        }

        public static void anuncioObtenido(DataFirst.Bot _ProduccionBot, int vIndiceAnuncio)
        {
            try
            {
                IWebElement we =
                    _ProduccionBot.
                        WebDriver.
                            FindElement
                                (
                            By.XPath(
                                "/html/body/div/div/main/div/section/div/div/div[5]/div[2]/div/div[2]/ul/li[" + vIndiceAnuncio.ToString() + "]/a"
                                )
                            );

                Aviso avisoNuevo = new Aviso();
                avisoNuevo.FHAlta = DateTime.Now;
                avisoNuevo.Url = we.GetAttribute("href");

                we =
                    _ProduccionBot.
                        WebDriver.
                            FindElement
                                (
                            By.XPath(
                                "/html/body/div/div/main/div/section/div/div/div[5]/div[2]/div/div[2]/ul/li[" + vIndiceAnuncio.ToString() + "]/a/div/span[3]"
                                )
                            );

                avisoNuevo.Titulo = we.Text;
                listaAvisos.Add(avisoNuevo);

                Console.WriteLine("Aviso recuperado en tag nro "+vIndiceAnuncio.ToString());
            }
            catch
            {
                Console.WriteLine("Aviso vacío en tag nro " + vIndiceAnuncio.ToString());
            }
        }

        public static void expandirAnuncios(DataFirst.Bot _ProduccionBot)
        {
            bool vExpandible = true;
            while(vExpandible)
            {
                try
                {
                    Console.WriteLine("Expandiendo anuncios...");
                    _ProduccionBot.WebDriver.ExecuteScript("window.scrollBy(0,600)");
                    IWebElement we =
                    _ProduccionBot.
                        WebDriver.
                            FindElement
                                (
                            By.XPath(
                                "/html/body/div/div/main/div/section/div/div/div[5]/div[2]/div/div[3]/button"
                                )
                            );
                    we.Click();
                    _ProduccionBot.WebDriver.ExecuteScript("window.scrollBy(0,600)");
                    vMultiplicadorLista++;
                    Thread.Sleep(3000);
                }
                catch
                {
                    vExpandible = false;
                }
            }
        }
        public static string vSpeech { get; set; }
        public static void setSpeech()
        {
            vSpeech = @"CONSULTORES TECNOLÓGICOS: PUBLICIDAD DIGITAL, SOLUCIONES IT, AUTOMATIZACIONES. SOFTWARE PARA EMPRESAS";
        }
    }
}
