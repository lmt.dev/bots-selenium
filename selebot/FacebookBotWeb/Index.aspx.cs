﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FacebookBotWeb
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                refrescar();
            }            
        }
        protected void refrescar()
        {
            List<Facebook.Post> listaPosts = new Facebook.Post()
                    .Select()
                        .Where(w1 => w1.FHAlta > DateTime.Now.AddMinutes(-60))
                            .OrderByDescending(or=>or.FHAlta)
                                .ToList();
            foreach (Facebook.Post postRecorrido in listaPosts)
            {
                if (postRecorrido.ImgSrc == null)
                {
                    postRecorrido.ImgSrc = "..\\img\\cubo.jpg";
                }
            }
            gvPost.DataSource = listaPosts.ToDataTable();
            gvPost.DataBind();
            upGridViewPost.Update();
        }
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            refrescar();
        }
    }
}