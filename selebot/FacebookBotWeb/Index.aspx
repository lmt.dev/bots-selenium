﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="FacebookBotWeb.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="container-fluid">
			<asp:UpdatePanel ID="upGridViewPost" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" style="margin-left: 10px; margin-right: 10px;">
		        <ContentTemplate>
                    <asp:GridView ID="gvPost" CssClass="table table-striped table-bordered table-hover grid-view"
                        Width="100%" AutoGenerateColumns="False" runat="server"
                        EmptyDataText="Sin datos para mostrar.">
                        <Columns>

                            <asp:TemplateField HeaderText="Foto">
                                <ItemTemplate>
                                     <asp:Image 
                                         ID="Image1" 
                                         ImageUrl='<%# Eval("ImgSrc") %>'
                                         DataTextField="ImgAlt"
                                         runat="server" Width="150" Height="150" />
                                </ItemTemplate>
                            </asp:TemplateField>

                           <asp:HyperLinkField
								DataNavigateUrlFields="GrupoLink,Permalink"
								DataNavigateUrlFormatString="https://www.facebook.com/{0}permalink/{1}"
                                Target="_blank"
								DataTextField="Contenido"
								HeaderText="Contenido"
								SortExpression="Contenido"
                               >
						   </asp:HyperLinkField>

                           <asp:HyperLinkField
								DataNavigateUrlFields="GrupoLink"
								DataNavigateUrlFormatString="https://www.facebook.com/{0}"
                                Target="_blank"
								DataTextField="GrupoNombre"
								HeaderText="Grupo"
								SortExpression="GrupoNombre"
                                HeaderStyle-Width="250"
                                ItemStyle-Width="250"
                               >                               
						   </asp:HyperLinkField>
                            <asp:HyperLinkField
								DataNavigateUrlFields="PerfilLink"
								DataNavigateUrlFormatString="https://www.facebook.com/{0}"
                                Target="_blank"
								DataTextField="PerfilNombre"
								HeaderText="Perfil"
								SortExpression="PerfilNombre"
                                HeaderStyle-Width="150"
                                ItemStyle-Width="150"
                                >
						    </asp:HyperLinkField>

                            <asp:TemplateField HeaderText="Avatar">
                                <ItemTemplate>
                                    <center>
                                         <asp:Image 
                                             ID="Image2" 
                                             ImageUrl='<%# Eval("PerfilFoto") %>'
                                             DataTextField="PerfilNombre"
                                             DataNavigateUrlField="PerfilLink"
                                             DataNavigateUrlFormatString="https://www.facebook.com/{0}"
                                             runat="server" Width="60" Height="60" />
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
		        </ContentTemplate>
	        </asp:UpdatePanel>
        </div>
    <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick">
    </asp:Timer>
</asp:Content>
