﻿using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using System;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Threading;

namespace DataFirst
{   //prueba logi 2
    //public int Id;
    //public IWebDriver WebDriver;
    public class Bot
    {
        public Error error { get; set; }
        public FirefoxDriver WebDriver { get; set; }
        public int Id { get; set; }
        public string UrlActual { get; set; }
        public Bet365.Proxy proxyBet365 { get ; set; }
        public string perfilNavegador { get; set; }

        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public bool HEADLESS = false;

        public void logInfo(string vLog)
        {
            log.Info(vLog);
        }

        public void imprimirImagen(string prefijo="Print")
        {
            try
            {
                Directory.CreateDirectory("C:\\Prints");
            }
            catch { }
            imprimirImagenSub(prefijo);
        }

        private void imprimirImagenSub(string prefijo)
        {
            Screenshot ss = ((ITakesScreenshot)WebDriver).GetScreenshot();
            string screenshot = ss.AsBase64EncodedString;
            byte[] screenshotAsByteArray = ss.AsByteArray;

            string archivoNombre =
                prefijo+"_" +
                    DateTime.Now.Year.ToString()
                    +
                    DateTime.Now.Month.ToString()
                    +
                    DateTime.Now.Day.ToString()
                    +
                    DateTime.Now.DayOfWeek.ToString()
                    +
                    DateTime.Now.Hour.ToString()
                    +
                    DateTime.Now.Minute.ToString()
                    +
                    DateTime.Now.Second.ToString()
                    + ".png"
                    ;
            ss.SaveAsFile("C:\\Prints\\" + archivoNombre);
        }

        public void logEx(string vLog, Exception ex, string vSistema)
        {
            try
            {
                error.Ingresar(vLog, ex, vSistema);
                log.Info(vLog+";Message: "+ex.Message+" ;StackTrace: "+ex.StackTrace);
            }
            catch(Exception exB)
            {
                Console.WriteLine("Error intentando registrar log: "+ exB.Message);
            }            
        }

        public void InicializarWebdriver()
        {
            if (WebDriver == null && licenciaActiva())
            {
                log.Info("Inicializando webdriver...");
                if (perfilNavegador == null || perfilNavegador == string.Empty)
                {
                    perfilNavegador = "default";
                }
                FirefoxOptions opciones = new FirefoxOptions();
                FirefoxProfileManager profileManager = new FirefoxProfileManager();
                FirefoxProfile profile = profileManager.GetProfile(perfilNavegador);

                var a = profileManager.ExistingProfiles;

                opciones.Profile = profile;
                if(HEADLESS)
                    opciones.AddArguments("--headless");
                WebDriver = new FirefoxDriver(opciones);
                WebDriver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(15);                
            }
        }

        public void ScrollToElement(IWebElement element) 
        {
            try //no anda esta poronga....revisar el error q no se está capturando.
            {
                Actions actions = new Actions(WebDriver);
                actions.MoveToElement(element);
                actions.Perform();
            }
            catch { }
            
        }

        public void IrA(string url)
        {
            try
            {
                log.Info("Ir a " + url);
                WebDriver.Navigate().GoToUrl(url);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public string InfoIp()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://ipinfo.io/?token=725314390c5b44");
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            JObject json;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                var a = reader.ReadToEnd();
                json = JObject.Parse(a);
            }
            var e = json["country"].ToString();
            var vTimeZone = json["timezone"].ToString();
            string[] vTimeZoneSplit = vTimeZone.Split('/');
            string vPais = vTimeZoneSplit[1];
            //ip  "181.165.194.225"
            //hostname    "225-194-165-181.fibertel.com.ar"
            //city    "Córdoba"
            //region  "Cordoba"
            //country "AR"
            //loc "-31.4135,-64.1810"
            //org "AS10318 Telecom Argentina S.A."
            //postal  "5000"
            //timezone    "America/Argentina/Cordoba"
            logInfo("navegando desde: " + json["city"] + " " + json["country"] + " con la ip '" + json["ip"] + "' del pais " + vPais);

            return e;
        }

        public bool EsperarPor(string xpath, int INTERVALO = 2, int TIMEOUT = -1, int REINTENTOS = 10, bool CLICKEAR = false, int SCROLLDOWN= 0, bool MOVETO = false, string ALTERNATIVEXPATH ="", bool JSCLICK = false)
        {
            bool rta = false;
            bool primera_vuelta = true;
            var tiempo_inicio = DateTime.Now;
            var scrollSIZE = 0;

            while (REINTENTOS > 0)
            {
                var tiempo_actual = DateTime.Now;
                try
                {
                    if ((tiempo_inicio - tiempo_actual).TotalSeconds > TIMEOUT && !primera_vuelta)
                    {
                        //rompero todo el loop por timeout
                        break;
                    }
                    if (SCROLLDOWN > 0)
                    {
                        while (true)
                        {
                            scrollSIZE = scrollSIZE + 350;
                            try
                            {
                                WebDriver.ExecuteScript("window.scrollBy(0," + scrollSIZE + ")");
                                WebDriver.FindElementByXPath(xpath);
                                break;
                            }
                            catch
                            {

                            }
                        }
                        SCROLLDOWN = SCROLLDOWN - 1;
                    }

                    //WebDriver.FindElementByXPath(xpath); descomentar si falla algo
                    if (MOVETO)
                    {
                        try
                        {
                            IJavaScriptExecutor js = (IJavaScriptExecutor)WebDriver;
                            string title = (string)js.ExecuteScript("arguments[0].scrollIntoView(true);", WebDriver.FindElementByXPath(xpath));
                            Thread.Sleep(500);
                        }
                        catch (Exception e)
                        {
                            
                        }
                        
                    }
                    if (JSCLICK)
                    {
                        IJavaScriptExecutor js = (IJavaScriptExecutor)WebDriver;
                        var script1 = "var a = document.evaluate(arguments[0], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;a.click();";
                        var a = js.ExecuteScript(script1, xpath);
                        if (a != null)
                        {
                            rta = true;
                            break;
                        }

                    }
                    else
                    {
                        if (CLICKEAR)
                            WebDriver.FindElementByXPath(xpath).Click();
                    }

                    WebDriver.FindElementByXPath(xpath);

                    rta = true; //salió todo bien
                    break;
                }
                catch
                {
                    Thread.Sleep(INTERVALO * 1000);
                    if (ALTERNATIVEXPATH != "")
                    {
                        try
                        {
                            WebDriver.FindElementByXPath(ALTERNATIVEXPATH);
                            rta = false; //salió todo bien pero al revez
                            break;
                        }
                        catch { }
                    }
                }

                REINTENTOS = REINTENTOS - 1;
                primera_vuelta = false;
            }

            return rta;
        }

        private bool licenciaActiva()
        {
            bool activa = true;
            //Luego hacer un validador de verdad (Ojo que esto aplica para todos los bots)
            if (DateTime.Now > Convert.ToDateTime("2020/06/01"))
            {
                activa = false;
            }
            return activa;
        }
    }
}
