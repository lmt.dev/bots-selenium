ALTER DATABASE Workana
SET RECOVERY SIMPLE;
GO
DBCC SHRINKFILE(Workana_Log, 0);
GO
ALTER DATABASE Workana
SET RECOVERY FULL;
GO