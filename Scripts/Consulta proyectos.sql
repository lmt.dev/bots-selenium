USE Workana;
select Postulado, IdProyecto,Titulo,Url,Pais,Puntos,FechaPublicado,Detalles,UltimoEstado
from Proyecto WITH (nolock)
where 1 = 1 -- IGNORAR ESTA LINEA
and (Postulado is null or UltimoEstado like '%Postulación fallida. Reintentar manualmente%')
order by Postulado desc, Puntos desc, FechaPublicado desc;
-- update proyecto set postulado = 1 where idProyecto in (2048); -- MARCAR POSTULACIÓN EN DETERMINADO PROYECTO
-- update proyecto set postulado = 0 where postulado is null or ultimoestado = 'Postulación fallida. Reintentar manualmente;'; -- MARCAR TODOS LOS PROYECTOS PENDIENTES COMO NO POSTULADO